<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resultado".
 *
 * @property int $inscripcion
 * @property int $codreunion
 * @property int $numprueba
 * @property int $coddeportista
 * @property string $marcadep
 * @property string $posdep
 *
 * @property Deportista $coddeportista0
 * @property Prueba $codreunion0
 * @property Prueba $numprueba0
 */
class Resultado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resultado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codreunion', 'numprueba', 'coddeportista'], 'integer'],
            [['marcadep', 'posdep'], 'string', 'max' => 255],
            [['coddeportista'], 'exist', 'skipOnError' => true, 'targetClass' => Deportista::className(), 'targetAttribute' => ['coddeportista' => 'coddep']],
            [['codreunion'], 'exist', 'skipOnError' => true, 'targetClass' => Prueba::className(), 'targetAttribute' => ['codreunion' => 'codreunion']],
            [['numprueba'], 'exist', 'skipOnError' => true, 'targetClass' => Prueba::className(), 'targetAttribute' => ['numprueba' => 'numprueba']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'inscripcion' => 'Inscripcion',
            'codreunion' => 'Codigo de reunion',
            'numprueba' => 'Numero de prueba',
            'coddeportista' => 'Codigo de deportista',
            'marcadep' => 'Marca deportista',
            'posdep' => 'Posdep',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoddeportista0()
    {
        return $this->hasOne(Deportista::className(), ['coddep' => 'coddeportista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodreunion0()
    {
        return $this->hasOne(Prueba::className(), ['codreunion' => 'codreunion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumprueba0()
    {
        return $this->hasOne(Prueba::className(), ['numprueba' => 'numprueba']);
    }
}
