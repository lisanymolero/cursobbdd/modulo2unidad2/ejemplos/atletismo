<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deportista".
 *
 * @property int $coddep
 * @property string $nomapedeportista
 * @property string $provinciadep
 * @property string $fechanacimientodep
 * @property string $dnidep
 * @property string $domicilio
 * @property int $codposdep
 * @property int $telefono
 *
 * @property Resultado[] $resultados
 */
class Deportista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deportista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechanacimientodep'], 'safe'],
            [['codposdep', 'telefono'], 'integer'],
            [['nomapedeportista', 'provinciadep', 'domicilio'], 'string', 'max' => 255],
            [['dnidep'], 'string', 'max' => 9],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'coddep' => 'Codigo',
            'nomapedeportista' => 'Nombre y apellido',
            'provinciadep' => 'Provincia',
            'fechanacimientodep' => 'Fecha de nacimiento',
            'dnidep' => 'Dni',
            'domicilio' => 'Domicilio',
            'codposdep' => 'Codigo Postal',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['coddeportista' => 'coddep']);
    }
}
