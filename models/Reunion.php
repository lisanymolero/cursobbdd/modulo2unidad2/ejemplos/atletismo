<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reunion".
 *
 * @property int $codreunion
 * @property string $fechareunion
 * @property string $lugarreunion
 * @property string $nombrereunion
 *
 * @property Prueba[] $pruebas
 */
class Reunion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reunion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechareunion'], 'safe'],
            [['lugarreunion', 'nombrereunion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codreunion' => 'Codigo reunion',
            'fechareunion' => 'Fecha',
            'lugarreunion' => 'Lugar',
            'nombrereunion' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['codreunion' => 'codreunion']);
    }
}
