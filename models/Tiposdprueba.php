<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiposdprueba".
 *
 * @property int $codtip
 * @property string $desttip
 *
 * @property Prueba[] $pruebas
 */
class Tiposdprueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tiposdprueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desttip'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codtip' => 'Codigo tipo',
            'desttip' => 'Desttip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPruebas()
    {
        return $this->hasMany(Prueba::className(), ['codtipo' => 'codtip']);
    }
}
