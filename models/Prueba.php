<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prueba".
 *
 * @property int $numprueba
 * @property int $codreunion
 * @property int $codtipo
 * @property string $horaprueba
 * @property string $lugarprueba
 *
 * @property Tiposdprueba $codtipo0
 * @property Reunion $codreunion0
 * @property Resultado[] $resultados
 * @property Resultado[] $resultados0
 */
class Prueba extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prueba';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codreunion', 'codtipo'], 'integer'],
            [['horaprueba'], 'safe'],
            [['lugarprueba'], 'string', 'max' => 255],
            [['codtipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tiposdprueba::className(), 'targetAttribute' => ['codtipo' => 'codtip']],
            [['codreunion'], 'exist', 'skipOnError' => true, 'targetClass' => Reunion::className(), 'targetAttribute' => ['codreunion' => 'codreunion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numprueba' => 'Numprueba',
            'codreunion' => 'Codigo reunion',
            'codtipo' => 'Codigo tipo',
            'horaprueba' => 'Hora',
            'lugarprueba' => 'Lugar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodtipo0()
    {
        return $this->hasOne(Tiposdprueba::className(), ['codtip' => 'codtipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodreunion0()
    {
        return $this->hasOne(Reunion::className(), ['codreunion' => 'codreunion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados()
    {
        return $this->hasMany(Resultado::className(), ['codreunion' => 'codreunion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultados0()
    {
        return $this->hasMany(Resultado::className(), ['numprueba' => 'numprueba']);
    }
}
