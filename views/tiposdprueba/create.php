<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tiposdprueba */

$this->title = 'Crear Tipos de prueba';
$this->params['breadcrumbs'][] = ['label' => 'Tiposdpruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiposdprueba-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
