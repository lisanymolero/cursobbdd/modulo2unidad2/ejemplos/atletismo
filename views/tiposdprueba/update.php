<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tiposdprueba */

$this->title = 'Actualizar Tipos de prueba: ' . $model->codtip;
$this->params['breadcrumbs'][] = ['label' => 'Tiposdpruebas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codtip, 'url' => ['view', 'id' => $model->codtip]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tiposdprueba-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
