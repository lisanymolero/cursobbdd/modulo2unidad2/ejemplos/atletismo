<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Reunion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reunion-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'fechareunion')->widget(DatePicker::className(), [
    'options' => ['class' => 'form-control'],
    'language' => 'es',
    'dateFormat' => 'yyyy-MM-dd',
    'clientOptions'=>['showAnim'=>'size','showButtonPanel'=>'true'],
]) ?>

    <?= $form->field($model, 'lugarreunion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombrereunion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
