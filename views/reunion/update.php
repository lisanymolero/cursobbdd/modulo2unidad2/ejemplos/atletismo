<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reunion */

$this->title = 'Actualizar Reunion: ' . $model->codreunion;
$this->params['breadcrumbs'][] = ['label' => 'Reunions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codreunion, 'url' => ['view', 'id' => $model->codreunion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reunion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
