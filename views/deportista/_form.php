<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Deportista */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deportista-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomapedeportista')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'provinciadep')->textInput(['maxlength' => true]) ?>

  
    
   <?= $form->field($model, 'fechanacimientodep')->widget(DatePicker::className(), [
    'options' => ['class' => 'form-control'],
    'language' => 'es',
    'dateFormat' => 'yyyy-MM-dd',
    'clientOptions'=>['showAnim'=>'size','showButtonPanel'=>'true'],
]) ?>

    <?= $form->field($model, 'dnidep')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'domicilio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codposdep')->textInput() ?>

    <?= $form->field($model, 'telefono')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
