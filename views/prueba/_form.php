<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Prueba */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prueba-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codreunion')->textInput() ?>

    <?= $form->field($model, 'codtipo')->textInput() ?>
    
<?= $form->field($model, 'horaprueba')->widget(DateTimePicker::className(), [
    'options' => ['class' => 'form-control'],
    'language' => 'es',
    'size' => 'ms',
    'pickButtonIcon' => 'glyphicon glyphicon-time',
    'inline' => false,
    'clientOptions' => [
        'startView' => 1,
        'minView' => 0,
        'maxView' => 1,
        'autoclose' =>true,
        'linkFormat' => 'HH:ii P', // if inline = true
        // 'format' => 'HH:ii P', // if inline = false
        'todayBtn' => true
    ]
]);?>

    <?= $form->field($model, 'lugarprueba')->textInput(['maxlength' => true]) ?>
    
    

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

   
</div>
